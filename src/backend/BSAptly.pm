#
# SPDX-License-Identifier: GPL-2.0+
#
# Copyright 2022 Collabora Ltd.

package BSAptly;

use BSConfiguration;
use BSPublisher::Util;
use BSRevision;
use BSUtil;
use Build::Deb;
use POSIX qw(strftime);

use strict;
# TODO: drop "no warnings" when upgraded to Perl 5.36
no warnings 'experimental::signatures';
use feature 'signatures';
use feature 'say';

our %aptly_config_projects;

BEGIN {
  # From the obsolete aptly configuration, generate hash mapping project and repositories
  # to the distribution where they live.
  say STDERR 'WARNING: Deprecated configuration option \$aptly_config used, please use $aptly_projects instead!'
    if defined $BSConfig::aptly_config;

  foreach my $prefix (keys %{ $BSConfig::aptly_config }) {
    my $distros = $BSConfig::aptly_config->{$prefix};
    foreach my $distro (keys %{ $distros }) {
      my $distro_config = $distros->{$distro};
      die "$prefix -> $distro does not have aptly-server defined" if !defined $distro_config->{'aptly-server'};

      my $distro_components = $distro_config->{'components'};
      foreach my $component (keys %{ $distro_components }) {
        my $component_config = $distro_components->{$component};
        $aptly_config_projects{$component_config->{'project'}}{$component_config->{'repository'}} = $distro_config;
        $distro_config->{'prefix'} = $prefix;
        $distro_config->{'distribution'} = $distro;
        $component_config->{'component'} = $component;
      }
    }
  }

  my $repo_components = {};

  foreach my $projid (keys %{$BSConfig::aptly_projects}) {
    my $repos = $BSConfig::aptly_projects->{$projid};
    foreach my $repoid (keys %{$repos}) {
      my $repo_config = $repos->{$repoid};
      if (defined $repo_config->{'target'}) {
        my $target_config = $BSConfig::aptly_targets->{$repo_config->{'target'}};
        die "$projid/$repoid refers to an undefined target repository $repo_config->{'target'}" if !defined $target_config;

        $repo_config->{'aptly-server'} ||= $target_config->{'server'};
        $repo_config->{'gpg-key'}      ||= $target_config->{'gpg-key'};
        $repo_config->{'prefix'}       ||= $target_config->{'prefix'};
      }

      die "$projid/$repoid does not have either aptly-server or target defined" if !defined $repo_config->{'aptly-server'};
      die "$projid/$repoid does not have prefix defined" if !defined $repo_config->{'prefix'};
      die "$projid/$repoid does not have distribution defined" if !defined $repo_config->{'distribution'};

      # OBS gives us a single OBS project/repoid, but we need to publish all projects in the same prefix at once
      # For this reason, we need to share the components set, which means that we (confusingly) have aptly
      # repo-specific configs that contain information about their sibling repos.
      my $all_components_in_prefix = ($repo_components
          ->{$repo_config->{'aptly-server'}}
          ->{$repo_config->{'prefix'}}
          ->{$repo_config->{'distribution'}}
          ->{'components'} ||= {});
      $all_components_in_prefix->{$repo_config->{'component'}} = {
        'project'    => $projid,
        'repository' => $repoid,
        'component'  => $repo_config->{'component'},
      };
      $repo_config->{'components'} = $all_components_in_prefix;
      $aptly_config_projects{$projid}{$repoid} = $repo_config;
    }
  }
}

sub api_tool_exec {
  my $aptly_server = shift;
  my @args = @_;
  my $pid;

  if (!defined($pid = fork())) {
    die "Failed to fork child: $!";
  } elsif ($pid == 0) {
    open(STDERR, '>&STDOUT');
    $ENV{'APTLY_API_URL'} = $aptly_server->{'url'};
    $ENV{'APTLY_API_TOKEN'} = $aptly_server->{'token'} if defined $aptly_server->{'token'};
    my $timestamp = strftime "%Y-%m-%d %H:%M:%S", gmtime;
    print STDOUT "$timestamp: ".join(' ', @args)."\n";
    exec(@args) || die("Failed to exec $args[0]: $!\n");
  } else {
    waitpid($pid, 0) == $pid || die("Failed on waitpid $pid: $!\n");
    return $?;
  }
}

sub aptlyctl_exec {
  my $aptly_server = shift;
  api_tool_exec($aptly_server, 'aptlyctl', @_);
}

sub obs2aptly_exec {
  my $aptly_server = shift;
  api_tool_exec($aptly_server, 'obs2aptly', @_);
}

# Aptly configuration helpers

sub aptly_project_get_config($projid) {
  return $aptly_config_projects{$projid};
}

sub aptly_repo_get_config($projid, $repoid) {
  return $aptly_config_projects{$projid}{$repoid};
}

sub aptly_repo_get_component($projid, $repoid) {
  my $repo_config = aptly_repo_get_config($projid, $repoid);
  my $distro_components = $repo_config->{'components'};
  foreach my $component (keys %$distro_components) {
    my $component_config = $distro_components->{$component};
    if ($component_config->{'project'} eq $projid && $component_config->{'repository'} eq $repoid) {
      return $component;
    }
  }
  return undef;
}

# OBS objects helpers

sub obs_get_project($projid) {
  return BSRevision::readproj_local($projid, 1);
}

sub obs_get_repo($projid, $repoid) {
  my $obs_proj = obs_get_project($projid);
  if (!$obs_proj || !$obs_proj->{'repository'}) {
    return undef;
  }
  foreach my $repo (@{ $obs_proj->{'repository'} }) {
    if ($repo->{'name'} eq $repoid) {
      return $repo;
    }
  }
  return undef;
}

sub obs_project_repos($proj) {
  # From an OBS project, get all its repositories names in a hash. Only those
  # repositories configured in aptly are included.
  my %ret;
  if ($proj && $proj->{'repository'}) {
    foreach my $repo (@{ $proj->{'repository'} }) {
      my $reponame = $repo->{'name'};
      next if (!aptly_repo_get_config($proj->{'name'}, $reponame));
      $ret{$reponame} = 1;
    }
  }
  return \%ret;
}

sub aptly_publish_repo($projid, $repoid) {
  # Given a prefix and distribution, publish a set of multi-component
  # repositories. A repository is included only if:
  # * it's defined in the aptly config.
  # * the publish flag is enabled on OBS for the related project/repository.
  # Note that, as this included multiple OBS repositories, the arches defined
  # for each one might differ. So the union of all the enabled arches is used
  # when publishing.
  my %arches;
  my @to_publish;

  my $repo_config = aptly_repo_get_config($projid, $repoid);
  my $repo_components = $repo_config->{'components'};
  my $prefix = $repo_config->{'prefix'};
  my $distribution = $repo_config->{'distribution'};

  foreach my $component (keys %$repo_components) {
    my $component_config = $repo_components->{$component};
    my $projid = $component_config->{'project'};
    my $repoid = $component_config->{'repository'};
    my $obs_proj = obs_get_project($projid);
    my $obs_repo = obs_get_repo($projid, $repoid);

    next unless $obs_repo;

    push @to_publish, "$projid/$repoid//$component";
    foreach my $arch (@{ $obs_repo->{'arch'} }) {
      if (BSUtil::enabled($repoid, $obs_proj->{'publish'}, 1, $arch)) {
        $arches{$arch} = 1;
      }
    }
  }

  if (!%arches) {
    return 0;
  }

  my $gpgkey = $repo_config->{'gpg-key'} || $BSConfig::aptly_defconfig->{'gpg-key'};

  my @args = ('publish', 'create', 'repo',
    (map {
        "--architecture=".Build::Deb::basearch($_)
    } (('source'), keys %arches)),
    "--distribution=$distribution",
    '--skip-contents',
    '--skip-bz2',
    $prefix, @to_publish);
  push @args, "--gpg-key=$gpgkey" if $gpgkey;
  return aptlyctl_exec($repo_config->{'aptly-server'}, @args);
}

# Aptly publish command wrappers

sub aptly_publish_drop_check($projid, $repoid) {
  my $repo_config = aptly_repo_get_config($projid, $repoid);
  my @args = ('publish', 'drop', '--ignore-if-missing',
    $repo_config->{'prefix'},
    $repo_config->{'distribution'});
  return aptlyctl_exec($repo_config->{'aptly-server'}, @args);
}

sub aptly_publish_update($projid, $repoid) {
  # Update an already published repository distribution
  my $repo_config = aptly_repo_get_config($projid, $repoid);
  my $gpgkey = $repo_config->{'gpg-key'} || $BSConfig::aptly_defconfig->{'gpg-key'};
  my @args = ('publish', 'update',
    '--skip-contents',
    '--skip-bz2',
    $repo_config->{'prefix'},
    $repo_config->{'distribution'});
  push @args, "--gpg-key=$gpgkey" if $gpgkey;
  return aptlyctl_exec($repo_config->{'aptly-server'}, @args);
}

# Aptly snapshot command wrappers

sub aptly_snapshot_create($projid, $repoid, $timestamp) {
  my $repo_config = aptly_repo_get_config($projid, $repoid);
  my @args = ('repo', 'snapshot',
    "$projid/$repoid",
    "$projid/$repoid/$timestamp");
  return aptlyctl_exec($repo_config->{'aptly-server'}, @args);
}

sub aptly_snapshot_exists($projid, $repoid, $timestamp) {
  # Check if aptly snapshot exists
  my $repo_config = aptly_repo_get_config($projid, $repoid);
  my @args = ('snapshot', 'test-exists', "$projid/$repoid/$timestamp");
  return aptlyctl_exec($repo_config->{'aptly-server'}, @args) == 0;
}

sub aptly_distro_snapshot($projid, $repoid, $timestamp) {
  # Create snapshots for all the repositories for the project/repository.

  my $repo_config = aptly_repo_get_config($projid, $repoid);
  my $repo_components = $repo_config->{'components'};
  my $prefix = $repo_config->{'prefix'};
  my $distribution = $repo_config->{'distribution'};

  foreach my $component (keys %$repo_components) {
    my $component_config = $repo_components->{$component};
    my $projid = $component_config->{'project'};
    my $repoid = $component_config->{'repository'};
    my $obs_proj = obs_get_project($projid);
    my $obs_repo = obs_get_repo($projid, $repoid);

    next unless $obs_repo;

    aptly_snapshot_create($component_config->{'project'}, $component_config->{'repository'}, $timestamp);
  }
}

sub aptly_publish_snapshot($projid, $repoid, $timestamp) {
  # Publish snapshots for the repositories in a corresponding aptly prefix/distribution.
  my %arches;
  my @to_publish;

  my $repo_config = aptly_repo_get_config($projid, $repoid);
  my $repo_components = $repo_config->{'components'};
  my $prefix = $repo_config->{'prefix'};
  my $distribution = $repo_config->{'distribution'};

  foreach my $component (keys %$repo_components) {
    my $component_config = $repo_components->{$component};
    my $projid = $component_config->{'project'};
    my $repoid = $component_config->{'repository'};
    my $obs_proj = obs_get_project($projid);
    my $obs_repo = obs_get_repo($projid, $repoid);

    next unless $obs_repo;
    next unless aptly_snapshot_exists($projid, $repoid, $timestamp);

    push @to_publish, "$projid/$repoid/$timestamp//$component";
    foreach my $arch (@{ $obs_repo->{'arch'} }) {
      if (BSUtil::enabled($repoid, $obs_proj->{'publish'}, 1, $arch)) {
        $arches{$arch} = 1;
      }
    }
  }

  if (!%arches) {
    return 0;
  }

  my $gpgkey = $repo_config->{'gpg-key'} || $BSConfig::aptly_defconfig->{'gpg-key'};
  my @args = ('publish', 'create', 'snapshot',
    (map {
        "--architecture=".Build::Deb::basearch($_)
    } (('source'), keys %arches)),
    "--distribution=$distribution/snapshots/$timestamp",
    '--skip-contents',
    '--skip-bz2',
    $prefix, @to_publish);
  push @args, "--gpg-key=$gpgkey" if $gpgkey;

  return aptlyctl_exec($repo_config->{'aptly-server'}, @args);
}

# Aptly repo command wrappers

sub aptly_repo_create($projid, $repoid, $distribution, $component) {
  my $repo_config = aptly_repo_get_config($projid, $repoid);
  my @args = ('repo', 'create',
    "--distribution=$distribution",
    "--component=$component",
    "$projid/$repoid");
  return aptlyctl_exec($repo_config->{'aptly-server'}, @args);
}

sub aptly_repo_drop($projid, $repoid) {
  # Remove aptly repository. Use --force flag to remove even if there's
  # a related snapshot based on this repo.
  my $repo_config = aptly_repo_get_config($projid, $repoid);
  my @args = ('repo', 'drop', '--force',
    "$projid/$repoid");
  return aptlyctl_exec($repo_config->{'aptly-server'}, @args);
}

# Hook functions for OBS

sub aptly_putproject($proj, $oldproj, $proj_config) {

  my $oldrepos = obs_project_repos($oldproj);
  my $newrepos = obs_project_repos($proj);

  foreach my $repoid (keys %{ $oldrepos }) {
    my $repo_config = $proj_config->{$repoid};
    if (!$newrepos->{$repoid}) {
      #aptly_publish_drop_check($proj->{'name'}, $repoid);
      aptly_repo_drop($proj->{'name'}, $repoid);
      aptly_publish_repo($proj->{'name'}, $repoid);
    }
  }

  foreach my $repoid (keys %{ $newrepos }) {
    my $repo_config = $proj_config->{$repoid};
    #aptly_publish_drop_check($proj->{'name'}, $repoid);
    if (!$oldrepos->{$repoid}) {
      my $component = aptly_repo_get_component($proj->{'name'}, $repoid);
      aptly_repo_create($proj->{'name'}, $repoid, $repo_config->{'distribution'}, $component);
    }
    aptly_publish_repo($proj->{'name'}, $repoid);
  }
}

sub aptly_delproject($proj, $proj_config) {
  my $repos = obs_project_repos($proj);

  foreach my $repoid (keys %{ $repos }) {
    my $repo_config = $proj_config->{$repoid};
    aptly_publish_drop_check($proj->{'name'}, $repoid);
    aptly_repo_drop($proj->{'name'}, $repoid);
    aptly_publish_repo($proj->{'name'}, $repoid);
  }
}

sub aptly_update_repo($projid, $repoid, $config, $extrep) {
  my $repo_config = aptly_repo_get_config($projid, $repoid);
  return obs2aptly_exec($repo_config->{'aptly-server'}, "$projid/$repoid", $extrep)
    || aptly_publish_update($projid, $repoid);
}

1;
