ARG INSTALLDIR=/obs
ARG DEBIAN_RELEASE=bullseye

FROM debian:$DEBIAN_RELEASE-slim as base
ENV LC_ALL=C.UTF-8
ARG DEBIAN_FRONTEND=noninteractive
ARG INSTALLDIR
ARG DEBIAN_RELEASE

# Enable backports for sphinxsearch
RUN [ "$DEBIAN_RELEASE" != bullseye ] || \
    echo "deb http://deb.debian.org/debian bullseye-backports main" > /etc/apt/sources.list.d/backports.list

RUN apt-get update \
 && apt-get install -y \
        apt-utils \
        adduser \
        ca-certificates \
        curl \
        diffutils \
        dpkg-dev \
        git \
        locales \
        libjs-bootstrap \
        make \
        msmtp-mta \
        mariadb-client \
        npm \
        patch \
        pkgconf \
        ruby2.7 \
        ruby2.7-dev \
        ruby-bundler \
        ruby-ffi \
        sphinxsearch \
        python3-yaml \
        supervisor \
        time \
        tzdata

RUN apt-get update \
 && apt-get install -y \
        default-libmysqlclient-dev \
        libffi-dev \
        libldap2-dev \
        libsasl2-dev \
        libxml2-dev \
        libxslt1-dev \
        zlib1g-dev

ADD src/api/Gemfile src/api/Gemfile.lock $INSTALLDIR/src/api/
WORKDIR $INSTALLDIR/src/api/

# Force Ruby 2.7 no matter what
RUN for bin in $(dpkg -L ruby | grep /usr/bin/); do \
      ln -sf ${bin}2.7 $bin; \
    done
RUN echo "ruby '~> 2.7.0'" >> Gemfile

# Drop the hard-coded Bundler version so we can use the distro-provided Bundler
RUN sed -e '/BUNDLED WITH/,+1 d' Gemfile.lock \
        -e 's/^  ruby$/  ruby '"$(ruby2.7 -v | cut -d' ' -f2)"'/' > Gemfile.lock.new; \
    diff -u Gemfile.lock Gemfile.lock.new; \
    mv Gemfile.lock.new Gemfile.lock

ARG NOKOGIRI_USE_SYSTEM_LIBRARIES=1

RUN bundle config --global without assets:development:test

RUN bundle config build.ffi --enable-system-libffi; \
    bundle config build.nokogiri --use-system-libraries; \
    bundle config build.nio4r --with-cflags='-Wno-return-type'

RUN bundle install --jobs=$(nproc) --retry=3 \
 && rm -rf \
    /var/lib/gems/*/cache/ \
    /var/lib/gems/*/test/ \
    /var/lib/gems/*/extensions/*/*/*/gem_make.out \
    /var/lib/gems/*/extensions/*/*/*/*.log

ENV RAILS_ENV=production
ENV RAILS_LOG_TO_STDOUT=true

FROM base as asset-builder
ARG INSTALLDIR

ADD src/api/ $INSTALLDIR/src/api/

COPY --from=base $INSTALLDIR/src/api/Gemfile $INSTALLDIR/src/api/Gemfile.lock $INSTALLDIR/src/api/

RUN rm -rf vendor/cache

ARG BUNDLE_BUILD__SASSC=--disable-march-tune-native
ARG NOKOGIRI_USE_SYSTEM_LIBRARIES=1

RUN gem install --no-format-executable brakeman --version 5.0.2 --no-doc
RUN gem install sassc --version 2.0.1 --no-doc

RUN bundle config build.ffi --enable-system-libffi; \
    bundle config build.nokogiri --use-system-libraries; \
    bundle config build.nio4r --with-cflags='-Wno-return-type'

RUN bundle config --local without development:test

RUN bundle install --jobs=$(nproc) --retry=3

RUN echo nonce > config/secret.key \
 && DATABASE_URL=mysql2://localhost/noncedb bundle exec rake assets:precompile RAILS_GROUPS=assets

FROM base
ARG INSTALLDIR

# Here, we end up with an image with runtime dependencies only, Gemfiles and pre-built assets
COPY --from=asset-builder $INSTALLDIR/src/api/public/assets  $INSTALLDIR/src/api/public/assets
