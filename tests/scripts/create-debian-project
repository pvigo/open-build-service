#!/bin/sh

debian_release=${1:-bullseye}

debian_prj_name="Debian:$debian_release:main"
debian_repo="main"
debian_url="http://deb.debian.org/debian/$debian_release/main"
prj_arch=x86_64

get_debianmeta() {
	cat << EOF
<project name="$debian_prj_name">
  <repository name="$debian_repo">
    <download arch="$prj_arch" url="$debian_url" repotype="deb"/>
    <arch>$prj_arch</arch>
  </repository>
</project>
EOF
}

get_debianconf() {
	cat << EOF
Repotype: debian
type: dsc
buildengine: debootstrap

Order: base-passwd:base-files

Preinstall: dash bash sed grep coreutils debianutils
Preinstall: libc6 libncurses5 libacl1 libattr1 libpcre3
Preinstall: libpcre2-8-0 libcrypt1
Preinstall: diffutils tar dpkg libc-bin
Preinstall: gzip base-files base-passwd
Preinstall: libselinux1 libsepol1
Preinstall: libgcc-s1 util-linux debconf tzdata findutils libdbus-1-3
Preinstall: liblzma5 xz-utils libstdc++6 passwd
Preinstall: login zlib1g libbz2-1.0 libtinfo5 libsigsegv2
Preinstall: dash insserv libgmp10 libdebconfclient0
Preinstall: perl-base perl libperl-dev mawk init-system-helpers

Required: build-essential apt mount fakeroot dpkg-dev ncurses-base hostname
Required: libtool

# Work around package looking up variations of localhost e.g. glibc tries to look up localhost.
Support: libnss-myhostname

Prefer: mawk
Prefer: cvs libesd0 libfam0 libfam-dev expect
Prefer: locales default-jdk
Prefer: xorg-x11-libs libpng fam mozilla mozilla-nss xorg-x11-Mesa
Prefer: unixODBC libsoup glitz java-1_4_2-sun gnome-panel
Prefer: desktop-data-SuSE gnome2-SuSE mono-nunit gecko-sharp2
Prefer: apache2-prefork openmotif-libs ghostscript-mini gtk-sharp
Prefer: glib-sharp libzypp-zmd-backend
Prefer: sysv-rc make
Prefer: libjack-jackd2-dev libsndio-dev
Prefer: pkg-config
Prefer: texlive-htmlxml libncurses-dev
Prefer: libavcodec58
Prefer: libsystemd0
Prefer: libtinfo-dev
Prefer: libavfilter7
Prefer: libfontconfig-dev
EOF
}

echo "Creating OBS project: $debian_prj_name"
get_debianmeta | osc meta prj "$debian_prj_name" -F -
get_debianconf | osc meta prjconf "$debian_prj_name" -F -
